import React from 'react'
import { find } from 'lodash/fp'
import data from 'cv/data.js'

import General from './Root/General.jsx'
import Home from './Root/Home.jsx'

import styles from './Root.css'

class Root extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = this.getCleanState(props, context)
  }

  getCleanState(props, context) {
    return {
      activeBase: null,
    }
  }

  renderBase() {
    const {
      state: {
        activeBase,
      }
    } = this

    if (!activeBase) {
      return null
    }

    const datum = find({id: activeBase})(data)

    return activeBase === 'home' ? <Home key='1' /> : <General key='1' {...datum} />
  }

  render() {
    const {
      state: {
        activeBase,
      }
    } = this

    return (
      <main className={styles.root}>
        {this.renderBase()}
      </main>
    )
  }
}

export default Root
