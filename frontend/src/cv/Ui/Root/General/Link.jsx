import React from 'react'

export default function(props) {
  const {
    name,
    url,
  } = props

  return (
    <a
      href={url}
      target='_blank'
    >
      {name}
    </a>
  )
}

