import React from 'react'

import styles from './TimeSpan.css'

export default function(props) {
  const {
    from,
    to,
  } = props

  return (
    <div className={styles.root}>
      {from} - {to}
    </div>
  )
}
