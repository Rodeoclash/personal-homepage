import $ from 'jquery'
import _ from 'lodash'

let size = {
  width: null,
  height: null,
}

function setSize() {
  const width = $(window).width()
  const height = $(window).height()
  size.width = width
  size.height = height
}

const debouncedSetSize = _.debounce(setSize, 100)

$(window).on('resize', debouncedSetSize)
setSize()

function getSize() {
  return size
}

function getCentreVector() {
  return {
    x: size.width / 2,
    y: size.height / 2,
  }
}

export {
  getSize,
  getCentreVector,
}
