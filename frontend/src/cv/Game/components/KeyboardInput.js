import { Component } from 'ces'

const KeyboardInput = Component.extend({
  name: 'keyboardInput',
  init: function () {
    this.keysPressed = {}
  },
})

export default KeyboardInput
