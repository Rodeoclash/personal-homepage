import { System }    from 'ces'
import Matter        from 'matter-js'

const element        = document.getElementById('game')

const Render = System.extend({

  init: function({engine}) {
    this.engine = engine
  },

  addedToWorld: function(world) {

    // Adds singular bodies to the matter-js world
    world.entityAdded('body').add(entity => {
      const body = entity.getComponent('body')
      Matter.World.add(this.engine.world, body.body)
    })

    // Adds multiple bodies to the matter-js world
    world.entityAdded('bodies').add(entity => {
      const bodies = entity.getComponent('bodies')
      Matter.World.add(this.engine.world, bodies.bodies)
    })

    this._super(world)
  },

  update: function(timestamp) {
    // noop
  }

})

export default Render
