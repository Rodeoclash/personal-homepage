.PHONY: sh setup

setup:
	docker-compose run frontend npm install

frontend-sh:
	docker-compose run frontend bash
